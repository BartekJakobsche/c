#include <cmath>
#include <string>
#include <iostream>
#include "wyj.h"

#ifndef UNTITLED_KWAD_H
#define UNTITLED_KWAD_H

class kwad {
public:
    double a;
    double b;
    double c;
    double d;
    double w[2];
    double t;


    kwad(double a, double b, double c) {
        this->a = a;
        this->b = b;
        this->c = c;
    }

    double miejsce(unsigned m) {
        wyj  wyj;
        d = b * b - 4 * a * c;
        if (d < 0) {
            std::cout << wyj.wyjatek << std::endl;
            throw wyj.wyjatek;
        } else if (d == 0) {
            w[0] = (-b / (2 * a));
            if (m > 1) { throw wyj.wyjatek1;}

        } else {
            w[0] = ((-b - sqrt(d)) / (2 * a));
            w[1] = ((-b + sqrt(d)) / (2 * a));
            if (w[0] > w[1]) {
                t = w[1];
                w[1] = w[0];
                w[0] = t;
            }if (m>2){throw wyj.wyjatek2;}
        }

        return w[m - 1];
    }
};


#endif //UNTITLED_KWAD_H
